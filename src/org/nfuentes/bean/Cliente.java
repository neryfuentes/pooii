
package org.nfuentes.bean;

/**
 *
 * @author Nery Fuentes
 */
public class Cliente {
    private String nit;
    private String dpi;
    private String nombre;
    private String direccion;
    private String telefono;
    private String emil;

    public Cliente() {
    }

    public Cliente(String nit, String dpi, String nombre, String direccion, String telefono, String emil) {
        this.nit = nit;
        this.dpi = dpi;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.emil = emil;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmil() {
        return emil;
    }

    public void setEmil(String emil) {
        this.emil = emil;
    }
    
    
    
    
}

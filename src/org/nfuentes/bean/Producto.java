
package org.nfuentes.bean;

/**
 *
 * @author Nery Fuentes
 */
public class Producto {
    
    private int codigoProducto;
    private String descripcion;
    private float precioUnitario;
    private float precioDocena;
    private float precioMayor;
    private int existencia;
    private String tipoEmpaque;
    private int codigoCategoria;

    public Producto() {
    }

    public Producto(int codigoProducto, String descripcion, float precioUnitario, float precioDocena, float precioMayor, int existencia, String tipoEmpaque, int codigoCategoria) {
        this.codigoProducto = codigoProducto;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.precioDocena = precioDocena;
        this.precioMayor = precioMayor;
        this.existencia = existencia;
        this.tipoEmpaque = tipoEmpaque;
        this.codigoCategoria = codigoCategoria;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(float precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public float getPrecioDocena() {
        return precioDocena;
    }

    public void setPrecioDocena(float precioDocena) {
        this.precioDocena = precioDocena;
    }

    public float getPrecioMayor() {
        return precioMayor;
    }

    public void setPrecioMayor(float precioMayor) {
        this.precioMayor = precioMayor;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public String getTipoEmpaque() {
        return tipoEmpaque;
    }

    public void setTipoEmpaque(String tipoEmpaque) {
        this.tipoEmpaque = tipoEmpaque;
    }

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }
    
    
    
}

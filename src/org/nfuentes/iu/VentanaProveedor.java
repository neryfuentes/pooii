/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.nfuentes.modelo.ModeloDatosProveedor;


/**
 *
 * @author APC
 */
public class VentanaProveedor extends VentanaCatalogo implements ActionListener{
    
    
    public VentanaProveedor(){
        super("Proveedores",650,550,new ModeloDatosProveedor());
        
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==getBtnSalir()) this.dispose();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.nfuentes.bean.Categoria;
import org.nfuentes.modelo.ModeloDatosCategoria;
/**
 *
 * @author APC
 */
public class VentanaCategoria extends VentanaCatalogo implements ActionListener{    
    public VentanaCategoria(){
        super("Categorias",650,550,new ModeloDatosCategoria());
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==getBtnAgregar()){
            new VentanaAgregarCategoria((ModeloDatosCategoria)getModelo());
        }else
        if(e.getSource()==getBtnEliminar()){
            
            //((ModeloDatosCategoria)getModelo()).getElemento(getTblDatos().getSelectedRow());
            ((ModeloDatosCategoria)getModelo()).eliminar(getTblDatos().getSelectedRow());            
            
        }else if(e.getSource()==getBtnSalir()) this.dispose();
        else if(e.getSource()==getBtnModificar()){
            if(getTblDatos().getSelectedRow()!=1){
                Categoria elemento=((ModeloDatosCategoria)getModelo()).getElemento(getTblDatos().getSelectedRow());
                new VentanaModificarCategoria((ModeloDatosCategoria)getModelo(),elemento);
            }
        }
    }
    
}

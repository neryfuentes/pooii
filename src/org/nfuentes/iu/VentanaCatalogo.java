/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

//import org.nfuentes.modelo.ModeloDatosCliente;
//import org.nfuentes.modelo.ModeloDatosCategoria;
//import org.nfuentes.modelo.ModeloDatosProducto;
//import org.nfuentes.modelo.ModeloDatosProveedor;

/**
 *
 * @author APC
 */
public class VentanaCatalogo extends JFrame implements ActionListener{
    private JButton btnAgregar;
    private JButton btnModificar;
    private JButton btnBorrar;
    private JButton btnBuscar;
    private JButton btnSalir;
    private JButton btnReporte;
    
    private JTable tblDatos ;
    private JScrollPane scrDatos;
    private AbstractTableModel modelo;
    //private ModeloDatosProducto modelo;
    //private ModeloDatosProveedor modelo;
    
    public VentanaCatalogo(String titulo,int ancho, int alto,AbstractTableModel modelo){
        this.modelo=modelo;
        this.setTitle(titulo);
        this.setSize(ancho,alto);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //this.setIconImage(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/usuario.png")).getImage());
        
        btnAgregar= new JButton("Agregar");
        btnAgregar.addActionListener(this);        
        btnAgregar.setBounds(10, 10, 120, 60);
        btnAgregar.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/add.png")));
        
        btnModificar=new JButton("Modificar");
        btnModificar.setBounds(10, 80, 120, 60);              
        btnModificar.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/edit.png")));
        btnModificar.addActionListener(this);
        
        btnBorrar=new JButton("Borrar");
        btnBorrar.setBounds(10, 150, 120, 60);
        btnBorrar.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/delete.png")));
        btnBorrar.addActionListener(this);
        
        btnBuscar=new JButton("Buscar");
        btnBuscar.setBounds(10, 220, 120, 60);
        btnBuscar.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/search.png")));
        btnBuscar.addActionListener(this);
        
        btnSalir=new JButton("Salir");
        btnSalir.setBounds(10, 360, 120, 60);
        btnSalir.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/exit.png")));
        btnSalir.addActionListener(this);
        
        btnReporte=new JButton("Reporte");
        btnReporte.setBounds(10, 290, 120, 60);
        btnReporte.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/report.png")));
        btnReporte.addActionListener(this);
        
        tblDatos= new JTable();
        //modelo=new ModeloDatosProveedor();
        tblDatos.setModel(modelo);
        
        scrDatos=new JScrollPane();
        scrDatos.setBounds(140,10,480,500);
       
        scrDatos.setViewportView(tblDatos);
        
                            
        this.getContentPane().add(btnAgregar);
        this.getContentPane().add(btnModificar);
        this.getContentPane().add(scrDatos);
        
        this.getContentPane().add(btnBorrar);
        this.getContentPane().add(btnBuscar);
        this.getContentPane().add(btnReporte);
        this.getContentPane().add(btnSalir);
                
        
        this.setVisible(true);
        
        
    }
    public JTable getTblDatos(){
        return tblDatos;
    }
    public AbstractTableModel getModelo(){
        return modelo;
    }
    
    public JButton getBtnModificar(){
        return btnModificar;
    }
    public JButton getBtnSalir(){
        return btnSalir;
    }
    public JButton getBtnAgregar(){
        return btnAgregar;
    }
    public JButton getBtnEliminar(){
        return btnBorrar;
    }
    
    public JButton getBtnBuscar(){
        return btnBuscar;
    }
    public JButton getBtnReporte(){
        return btnReporte;
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==btnSalir) dispose();
    }
    
}

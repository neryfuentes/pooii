/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;

import org.nfuentes.modelo.ModeloDatosCliente;
/**
 *
 * @author APC
 */
public class VentanaCliente extends VentanaCatalogo implements ActionListener{

    
    public VentanaCliente(){        
        super("Clientes",650,550,new ModeloDatosCliente());
                
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==getBtnSalir()) this.dispose();
    }
    
}

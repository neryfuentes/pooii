/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.nfuentes.modelo.ModeloDatosProducto;
/**
 *
 * @author APC
 */
public class VentanaProducto extends VentanaCatalogo implements ActionListener{

    
    public VentanaProducto(){
        super("Producto",650,550,new ModeloDatosProducto());                      
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==getBtnSalir()) this.dispose();
    }
    
}

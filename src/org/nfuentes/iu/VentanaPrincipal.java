/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import org.nfuentes.iu.VentanaCategoria;
import org.nfuentes.iu.VentanaInicioSesion;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import org.nfuentes.db.Conexion;

/**
 *
 * @author APC
 */
public class VentanaPrincipal extends JFrame implements ActionListener {
    private JDesktopPane panelVentana;
    private JMenuBar menuPrincipal;
    private JMenu catalogos;
    private JMenu login;
    private JMenuItem categoria;
    private JMenuItem cliente;
    private JMenuItem producto;
    private JMenuItem proveedor;
    
    private JMenuItem cerrarSesion;
    private JMenuItem iniciarSesion;
    
    public VentanaPrincipal(){
        this.setTitle("Punto de venta 2015");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/usuario.png")).getImage());
        panelVentana =new JDesktopPane(){
            @Override
            public void paintComponent(Graphics g){
                g.drawImage(
                        new ImageIcon(getClass().getResource("/org/nfuentes/recursos/fondo.jpg")).getImage(), 
                        0, 0, getWidth(),getHeight(),this);
            }
        };
        this.getContentPane().add(panelVentana,BorderLayout.CENTER);
        this.setJMenuBar(getMenuPrincipal());
        habilitarBotones(false);
        this.setVisible(true);
    }
    public JMenuBar getMenuPrincipal(){
        if(menuPrincipal==null) menuPrincipal =new JMenuBar();
        menuPrincipal.add(getLogin());
        menuPrincipal.add(getCatalogos());
        return menuPrincipal;
    }
    public void habilitarBotones(boolean habilitar){        
        this.catalogos.setEnabled(habilitar);
        this.cerrarSesion.setEnabled(habilitar);
        this.iniciarSesion.setEnabled(!habilitar);
    }
    public JMenu getCatalogos(){
        if(catalogos==null) catalogos =new JMenu("Catalogos");
        catalogos.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/catalogos.png")));
        catalogos.setMnemonic('C');
        catalogos.setToolTipText("Catalogos");
        catalogos.add(getCategoria());
        catalogos.add(getCliente());
        catalogos.add(getProducto());
        catalogos.add(getProveedor());       
        
        return catalogos;
    }
    public JMenuItem getCategoria(){
        if(categoria==null) categoria=new JMenuItem("Categoria");
        categoria.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/categoria.png")));
        categoria.setMnemonic('a');
        categoria.setToolTipText("Categorias");
        categoria.addActionListener(this);
        return categoria;
    }

    public JMenuItem getCliente() {
        if(cliente==null) cliente =new JMenuItem("Cliente");
        cliente.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/cliente32.png")));
        cliente.setMnemonic('i');
        cliente.setToolTipText("Clientes");        
        cliente.addActionListener(this);
        return cliente;
    }

    public JMenuItem getProducto() {
        if(producto==null) producto =new JMenuItem("Producto");
        producto.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/producto32.png")));
        producto.setMnemonic('p');
        producto.setToolTipText("Productos");
        producto.addActionListener(this);
        
        return producto;
    }

    public JMenuItem getProveedor() {
        if(proveedor==null) proveedor =new JMenuItem("Proveedor");
        proveedor.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/proveedor32.png")));
        proveedor.setMnemonic('e');
        proveedor.setToolTipText("Proveedores");  
        proveedor.addActionListener(this);
        
        return proveedor;
    }

    public JMenuItem getCerrarSesion() {
        if(cerrarSesion==null) cerrarSesion =new JMenuItem("Cerrar Sesion");
        cerrarSesion.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/cerrarsesion32.png")));
        cerrarSesion.setMnemonic('S');
        cerrarSesion.setToolTipText("Cerrar Sesion");   
        cerrarSesion.addActionListener(this);        
        return cerrarSesion;
    }

    public JMenuItem getIniciarSesion() {
        if(iniciarSesion==null) iniciarSesion =new JMenuItem("Iniciar Sesion");
        iniciarSesion.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/iniciarsesion32.png")));
        iniciarSesion.setMnemonic('o');
        iniciarSesion.setToolTipText("Iniciar Sesion");
        iniciarSesion.addActionListener(this);
        return iniciarSesion;
    }
    
    public JMenu getLogin(){
        if(login==null) login =new JMenu("Login");
        login.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/login32.png")));
        login.setMnemonic('l');
        login.setToolTipText("Login");        
        login.add(getIniciarSesion());
        login.add(getCerrarSesion());
        
        
        
        return login;
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==categoria){
            new VentanaCategoria();
        }
        if(e.getSource()==cliente){
            new VentanaCliente();
        }
        if(e.getSource()==producto){
            new VentanaProducto();
        }
        if(e.getSource()==proveedor){
            new VentanaProveedor();
        }
        
        
        if(e.getSource()==iniciarSesion){
            new VentanaInicioSesion(this);
        }
        if(e.getSource()==cerrarSesion){
            Conexion.cerrarSesion();
            habilitarBotones(false);
        }
    }
}

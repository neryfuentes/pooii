/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;

/**
 *
 * @author APC
 */
import org.nfuentes.iu.VentanaModificar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.nfuentes.bean.Categoria;
import org.nfuentes.modelo.ModeloDatosCategoria;


public class VentanaModificarCategoria extends VentanaModificar implements ActionListener{
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosCategoria modelo;
    private Categoria categoria;
    
    public VentanaModificarCategoria(ModeloDatosCategoria modelo,Categoria categoria){
        super("Modificar categoria",650,550);
        
        this.modelo=modelo;
        this.categoria=categoria;
        lblDescripcion=new JLabel("Descripcion");
        lblDescripcion.setBounds(150,10,100,20);
        txtDescripcion =new JTextField();
        txtDescripcion.setBounds(250,10,200,20);
        txtDescripcion.setText(categoria.getDescripcion());
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion); 
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==getBtnGuardar()){
            categoria.setDescripcion(txtDescripcion.getText());
            modelo.modificar(categoria);
         //modelo.agregar(new Categoria(0,txtDescripcion.getText()));
         this.dispose();
        }
        if(e.getSource()==getBtnCancelar()){
            this.dispose();
        }
    }
}

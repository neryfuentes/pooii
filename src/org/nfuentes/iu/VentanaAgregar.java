/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JButton;
/**
 *
 * @author APC
 */
public class VentanaAgregar extends JDialog implements ActionListener {
    private JButton btnGuardar;
    private JButton btnCancelar;
    
    public VentanaAgregar(String titulo,int ancho, int alto){        
        this.setTitle(titulo);
        this.setSize(ancho,alto);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        btnGuardar= new JButton("Agregar");
        btnGuardar.addActionListener(this);        
        btnGuardar.setBounds(10, 10, 120, 60);
        btnGuardar.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/add.png")));
        
        btnCancelar=new JButton("Cancelar");
        btnCancelar.setBounds(10, 80, 120, 60);
        btnCancelar.setIcon(new ImageIcon(getClass().getResource("/org/nfuentes/recursos/exit.png")));
        btnCancelar.addActionListener(this);
        
        this.getContentPane().add(btnGuardar);
        this.getContentPane().add(btnCancelar);
        this.setVisible(true);
    }
    public JButton getBtnGuardar(){
        return btnGuardar;
    }
    
    public JButton getBtnCancelar(){
        return btnCancelar;
    }
    
    public void actionPerformed(ActionEvent e){
    }
    
}

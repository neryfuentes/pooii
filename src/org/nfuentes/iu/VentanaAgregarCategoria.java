/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.nfuentes.bean.Categoria;
import org.nfuentes.modelo.ModeloDatosCategoria;
/**
 *
 * @author APC
 */
public class VentanaAgregarCategoria extends VentanaAgregar implements ActionListener{
    private JLabel lblDescripcion;
    private JTextField txtDescripcion;
    private ModeloDatosCategoria modelo;
    
    public VentanaAgregarCategoria(ModeloDatosCategoria modelo){
        super("Agregar categoria",650,200);
        this.modelo=modelo;
        lblDescripcion=new JLabel("Descripcion");
        lblDescripcion.setBounds(150,10,100,20);
        txtDescripcion =new JTextField();
        txtDescripcion.setBounds(250,10,200,20);
        this.getContentPane().add(lblDescripcion);
        this.getContentPane().add(txtDescripcion);                  
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==getBtnGuardar()){
         modelo.agregar(new Categoria(0,txtDescripcion.getText()));
         this.dispose();
        }
        if(e.getSource()==getBtnCancelar()){
            this.dispose();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.manejadores;
import java.util.ArrayList;
import org.nfuentes.bean.Categoria;
import org.nfuentes.db.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author APC
 */
public class ManejadorDeCategoria {
    private ArrayList<Categoria> lista=new ArrayList<Categoria>();

    public ArrayList<Categoria> getLista() {
        ResultSet datos=Conexion.getInstancia().hacerconsulta("select * from categoria");
        try{
            while(datos.next()){
                lista.add(new Categoria(
                        datos.getInt("codigo"),
                        datos.getString("descripcion")
                        
                ));
            }
        }
        
        catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
    
    public void agregar(Categoria categoria){
        //Conexion.getInstancia().agregarCategoria(categoria.getDescripcion());
        Map params =new HashMap();
        params.put("descripcion", categoria.getDescripcion());
                
        Conexion.getInstancia().ejecutarProcedimiento("execute sp_inserta_categoria ?", params, false);
    }
    public void eliminar(Categoria categoria){
        Map params =new HashMap();
        params.put("codigo", categoria.getCodigo());
                
        Conexion.getInstancia().ejecutarProcedimiento("execute sp_eliminar_categoria ?", params, false);
    }
    public void modificar(Categoria categoria){
        Map params =new HashMap();
        params.put("codigo", categoria.getCodigo());
        params.put("descripcion", categoria.getDescripcion());
        //System.out.println(categoria.getCodigo());
        //System.out.println(categoria.getDescripcion());
        Conexion.getInstancia().ejecutarProcedimiento("execute sp_modificar_categoria ?,?", params, false);
    }
    
    
}

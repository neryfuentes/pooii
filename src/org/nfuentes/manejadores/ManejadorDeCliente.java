package org.nfuentes.manejadores;

import java.util.ArrayList;
import org.nfuentes.bean.Cliente;
import org.nfuentes.db.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Nery Fuentes
 */
public class ManejadorDeCliente {
    
    private ArrayList<Cliente> lista=new ArrayList<Cliente>();

    public ArrayList<Cliente> getLista() {
        ResultSet datos=Conexion.getInstancia().hacerconsulta("select * from cliente");
        try{
            while(datos.next()){
                lista.add(new Cliente(
                        datos.getString("nit"),
                        datos.getString("dpi"),
                        datos.getString("nombre"),
                        datos.getString("direccion"),
                        datos.getString("telefono"),
                        datos.getString("emil")
                        
                ));
            }
        }
        
        catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
    
    
}

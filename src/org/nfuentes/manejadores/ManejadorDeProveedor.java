
package org.nfuentes.manejadores;

/**
 *
 * @author Nery Fuentes
 */

import java.util.ArrayList;
import org.nfuentes.bean.Proveedor;
import org.nfuentes.db.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ManejadorDeProveedor {
    private ArrayList<Proveedor> lista=new ArrayList<Proveedor>();

    public ArrayList<Proveedor> getLista() {
        ResultSet datos=Conexion.getInstancia().hacerconsulta("select * from proveedor");
        try{
            while(datos.next()){
                lista.add(new Proveedor(
                        datos.getInt("codigo"),
                        datos.getString("nit"),
                        datos.getString("razonSocial"),
                        datos.getString("direccion"),
                        datos.getString("telefono"),
                        datos.getString("paginaWeb"),
                        datos.getString("email")
                        
                ));
            }
        }
        
        catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}

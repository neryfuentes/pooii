
package org.nfuentes.manejadores;
import java.util.ArrayList;
import org.nfuentes.bean.Producto;
import org.nfuentes.db.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author Nery Fuentes
 */
public class ManejadorDeProducto {
    private ArrayList<Producto> lista=new ArrayList<Producto>();

    public ArrayList<Producto> getLista() {
        ResultSet datos=Conexion.getInstancia().hacerconsulta("select * from producto");
        try{
            while(datos.next()){
                lista.add(new Producto(
                        datos.getInt("codigoProducto"),
                        datos.getString("descripcion"),
                        datos.getFloat("precioUnitario"),
                        datos.getFloat("precioDocena"),
                        datos.getFloat("precioMayor"),
                        datos.getInt("existencia"),
                        datos.getString("tipoEmpaque"),
                        datos.getInt("codigoCategoria")
                        
                ));
            }
        }
        
        catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}

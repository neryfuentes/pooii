/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.modelo;

/**
 *
 * @author APC
 */
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.nfuentes.manejadores.ManejadorDeProducto;
import org.nfuentes.bean.Producto;

public class ModeloDatosProducto extends AbstractTableModel{
    private String[] encabezado={"Codigo","Descripcion","Precio Unitario","precio docena","precio mayor","existencia","Tipo Empaque","codigoCategoria"};
    private ArrayList<Producto> listaClientes=null;
    private ManejadorDeProducto manejador= new ManejadorDeProducto();    
    
    public ModeloDatosProducto(){
        listaClientes=manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezado[columna];
    }
    public int getColumnCount(){
        return encabezado.length;
    }
    public int getRowCount(){
        return listaClientes.size();
    }
    public String getValueAt(int fila,int columna){
        String resultado="";
        Producto elemento=listaClientes.get(fila);
        switch(columna){
            case 0:resultado=String.valueOf(elemento.getCodigoProducto()); break;
            case 1: resultado=elemento.getDescripcion(); break;
            case 2:resultado=String.valueOf(elemento.getPrecioUnitario()); break;
            case 3:resultado=String.valueOf(elemento.getPrecioDocena()); break;
            case 4:resultado=String.valueOf(elemento.getPrecioMayor()); break;
            case 5:resultado=String.valueOf(elemento.getExistencia()); break;
            case 6:resultado=String.valueOf(elemento.getTipoEmpaque()); break;
            case 7:resultado=String.valueOf(elemento.getCodigoCategoria()); break;                        
            
        };
        
        return resultado;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.modelo;

/**
 *
 * @author APC
 */

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.nfuentes.manejadores.ManejadorDeProveedor;
import org.nfuentes.bean.Proveedor;

public class ModeloDatosProveedor extends AbstractTableModel{
    private String[] encabezado={"NIT","Razon Social","Dirección","Telefono","Pagina Web","Email"};
    private ArrayList<Proveedor> listaClientes=null;
    private ManejadorDeProveedor manejador= new ManejadorDeProveedor();    
    
    public ModeloDatosProveedor(){
        listaClientes=manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezado[columna];
    }
    public int getColumnCount(){
        return encabezado.length;
    }
    public int getRowCount(){
        return listaClientes.size();
    }
    public String getValueAt(int fila,int columna){
        String resultado="";
        Proveedor elemento=listaClientes.get(fila);
        switch(columna){
            case 0: resultado=elemento.getNit(); break;
            case 1: resultado=elemento.getRazonSocial(); break;
            case 2: resultado=elemento.getDireccion(); break;
            case 3: resultado=elemento.getTelefono(); break;
            case 4: resultado=elemento.getPaginaWeb(); break;
            case 5: resultado=elemento.getPaginaWeb(); break;
            
        };
        
        return resultado;
    }
    
}

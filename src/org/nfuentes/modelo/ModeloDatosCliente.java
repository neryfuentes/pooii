/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.nfuentes.manejadores.ManejadorDeCliente;
import org.nfuentes.bean.Cliente;
/**
 *
 * @author APC
 */
public class ModeloDatosCliente extends AbstractTableModel {
    private String[] encabezado={"NIT","Nombre","Telefono","Direccion","DPI","Email"};
    private ArrayList<Cliente> listaClientes=null;
    private ManejadorDeCliente manejador= new ManejadorDeCliente();    
    
    public ModeloDatosCliente(){
        listaClientes=manejador.getLista();
    }
    public String getColumnName(int columna){
        return encabezado[columna];
    }
    public int getColumnCount(){
        return encabezado.length;
    }
    public int getRowCount(){
        return listaClientes.size();
    }
    public String getValueAt(int fila,int columna){
        String resultado="";
        Cliente elemento=listaClientes.get(fila);
        switch(columna){
            case 0:resultado=elemento.getNit(); break;
            case 1: resultado=elemento.getNombre(); break;
            case 2: resultado=elemento.getTelefono(); break;
            case 3: resultado=elemento.getDireccion(); break;
            case 4: resultado=elemento.getDpi(); break;
            case 5: resultado=elemento.getEmil(); break;
        };
        
        return resultado;
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nfuentes.modelo;

/**
 *
 * @author APC
 */

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.nfuentes.manejadores.ManejadorDeCategoria;
import org.nfuentes.bean.Categoria;

public class ModeloDatosCategoria extends AbstractTableModel{
    private String[] encabezado={"Codigo","Descripcion"};
    private ArrayList<Categoria> listaClientes=null;
    private ManejadorDeCategoria manejador= new ManejadorDeCategoria();    
    
    public ModeloDatosCategoria(){
        listaClientes=manejador.getLista();
    }
    @Override
    public String getColumnName(int columna){                
        return encabezado[columna];
    }
    @Override
    public int getColumnCount(){
        return encabezado.length;
    }
    
    @Override
    public int getRowCount(){
        return listaClientes.size();
    }
    public String getValueAt(int fila,int columna){
        String resultado="";
        Categoria elemento=listaClientes.get(fila);
        switch(columna){
            case 0:resultado=String.valueOf(elemento.getCodigo()); break;
            case 1: resultado=elemento.getDescripcion(); break;
            
        };
        
        return resultado;
    }
    public Categoria getElemento(int fila){
        return listaClientes.get(fila);
    }
    
    public void agregar(Categoria categoria){
        manejador.agregar(categoria);
        listaClientes.removeAll(listaClientes);
        listaClientes=manejador.getLista();
        
        //listaClientes.add(categoria);
        fireTableDataChanged();
        
    }
    
    public void eliminar(int fila){
        manejador.eliminar(listaClientes.get(fila));
        listaClientes.removeAll(listaClientes);
        listaClientes=manejador.getLista();
        
        //listaClientes.add(categoria);
        fireTableDataChanged();
    }
    public void modificar(Categoria categoria){
        manejador.modificar(categoria);
        listaClientes.removeAll(listaClientes);
        listaClientes=manejador.getLista();
        
        //listaClientes.add(categoria);
        fireTableDataChanged();
    }
}

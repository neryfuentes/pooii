
package org.nfuentes.db;

/**
 *
 * @author Nery Fuentes
 */

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Map;

public class Conexion {
    private static String user;
    private static String pass;
    private Connection conexion;
    private Statement sentencia;
    private static Conexion instancia;
    
    public static Conexion getInstancia(){
        if(instancia==null) instancia =new Conexion();        
        return instancia;
    }
    public void setUser(String user){
        this.user=user;        
    }
    public void setPass(String pass){
        this.pass=pass;
    }
    //esto lo deje privado para que no se pueda accesar desde otra clase a estos datos
    private String getUser(){
        return user;
    }
    private String getPass(){
        return pass;
    }
    
    public static void setInstancia(String usuario,String password){
        user=usuario;
        pass=password;
        instancia=new Conexion();
        
    }
    public static void cerrarSesion(){
        instancia=null;
    }
    
    public Conexion(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();            
            conexion=DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=ControlVentas;user="+getUser()+";password="+getPass()+";");
            sentencia=conexion.createStatement();
        }
        catch(SQLException e){
            e.printStackTrace();;
            return;
        }
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();            
        }
        

        
    }
    
    public ResultSet hacerconsulta(String consulta){
        ResultSet resultado=null;
        
        try{
            resultado=sentencia.executeQuery(consulta);                                
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    public ResultSet ejecutarProcedimiento(String nombreSP,Map params,boolean respuesta){
        ResultSet resultado=null;
        try{
            CallableStatement procedimiento =conexion.prepareCall(nombreSP);
            Iterator iterador=params.entrySet().iterator();
            while(iterador.hasNext()){
                Map.Entry e=(Map.Entry)iterador.next();
                String clase =e.getValue().getClass().toString();
                int longitud=clase.length();
                String tipoDato=clase.substring(clase.lastIndexOf(".")+1,longitud);
                //System.out.println(tipoDato);
                if(tipoDato.equals("String"))
                    procedimiento.setString(e.getKey().toString(),e.getValue().toString());
                else if(tipoDato.equals("Integer"))
                    procedimiento.setInt(e.getKey().toString(),Integer.parseInt(e.getValue().toString()));
                else if(tipoDato.equals("Double"))
                    procedimiento.setDouble(e.getKey().toString(),Double.parseDouble(e.getValue().toString()));
                else if(tipoDato.equals("Boolean"))
                    procedimiento.setBoolean(e.getKey().toString(),Boolean.parseBoolean(e.getValue().toString()));                                              
            }
            if(respuesta)
                resultado=procedimiento.executeQuery();
            else procedimiento.execute();
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
            
    public void agregarCategoria(String descripcion){        
        try{
            CallableStatement procedimiento =conexion.prepareCall("execute sp_inserta_categoria ?");
            procedimiento.setString("descripcion", descripcion);
            procedimiento.execute();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
}
